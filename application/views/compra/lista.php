<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Listado De Compras</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>Codigo</th>
                  <th>Fecha</th>
                  <th>Proveedor</th>
                  <th>Empleado</th>
                  <th>Acciones</th>
                </tr>                
          				<?php
          					for ($i = 0; $i < count($compras); $i++) {		
          				?>
                <tr class="clickable-row" data-href=".collapse-<?php echo $compras[$i]->codigo;?>">
                  <td><?php echo $compras[$i]->codigo;?></td>
                  <td><?php echo $compras[$i]->fecha;?></td>
                  <td><?php echo $compras[$i]->proveedor;?></td>
                  <td><?php echo $compras[$i]->empleado;?></td>
                  <td><button onclick="window.location.href='../ArticuloController/update?idArticulo=<?php echo $compras[$i]->codigo;?>'" class="btn btn-warning glyphicon glyphicon-pencil btn-xs"></button>
                        <button onclick="confirmDelete(<?php echo $compras[$i]->codigo;?>);" class="btn btn-danger glyphicon glyphicon-trash btn-xs" style="margin-left: 10px;"></button>
                  </td>
                </tr>
                <tr style="display: none; margin-left: 10px;" class="collapse-<?php echo $compras[$i]->codigo;?>">
                    <th>Articulo</th>
                    <th>Cantidad</th>
                </tr>
                    <?php for ($j=0; $j < count($compras[$i]->detalle); $j++) {?>
                <tr style="display: none; margin-left: 50px;" class="collapse-<?php echo $compras[$i]->codigo;?>">
                    <td><?php echo $compras[$i]->detalle[$j]->articulo;?></td>
                    <td><?php echo $compras[$i]->detalle[$j]->cantidad;?></td>
                </tr>
                    <?php }} ?>

              </table>
            </div>
          </div>
        </div>
</div>
<script src="<?php echo base_url('public/plugins/jQuery/jquery-2.2.3.min.js')?>"></script>
<script src="<?php echo base_url('public/js/validateCompra.js')?>"></script>