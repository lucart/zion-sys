function validate(id) {
  	var campos = "";
  	
  	if(!$("#nombre").val())
  		campos = campos + "Nombre \n";
    if(!$("#apellido").val())
      campos = campos + "Apellido \n";
    if(!$("#dni").val())
      campos = campos + "Dni \n";
    if(!$("#fecha_nacimiento").val())
      campos = campos + "Fecha de Nacimiento \n";
    if(!$("#calle").val())
      campos = campos + "Calle \n";
    if(!$("#numero").val())
      campos = campos + "Numero \n";
    if(!$("#codigo_postal").val())
      campos = campos + "Codigo Postal \n";
    if(!$("#cuil").val())
      campos = campos + "Cuil \n";
    if(!$("#fecha_ingreso").val())
      campos = campos + "Fecha de Ingreso \n";
  	if($("#provincia").val() == 0)
  		campos = campos + "Provincia \n";
    if($("#localidad").val() == 0)
      campos = campos + "Localidad \n";
    if($("#cargo").val() == 0)
      campos = campos + "Cargo \n";
  	
  	if(campos.length === 0){
  		$('form#guardarEmpleadoForm').submit();
  	}else{
  		swal("Completar los siguientes campos: ", campos);
  	}
}

function confirmDelete(id) {
    swal({
        title: "Eliminar Empleado",
        text: "¿Está seguro que desea eliminar este empleado?",
        type: "error",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Eliminar!',
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true
      },
      function(isConfirm){
        if (isConfirm) {
              window.location.href = "../EmpleadoController/eliminarEmpleado?idEmpleado=" + id;
        }
    });
}

function cargarLocalidades( ){
  $.ajax({
      type: "POST",
      url: "getPaises.php",
      success: function(response)
      {
          $('.selector-pais select').html(response).fadeIn();
      }
  });

    var provincia = document.getElementById("provincia").value;
    var localidad = document.getElementById("localidad");
    localidad.options.length =0;
    
    <?php for($i = 0; $i < count($localidades); $i++){?>
      if(<?php echo $localidades[$i]->provincia; ?> == provincia){        
        var option = document.createElement("option");
        option.value = <?php echo $localidades[$i]->codigo; ?>;
        option.text = <?php echo "'".$localidades[$i]->nombre."'"; ?>;
        localidad.add(option);
      }     
    <?php }?>
  } 