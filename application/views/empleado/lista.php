<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Listado De Empleados</h3>

        <div class="box-tools">
          <div class="input-group input-group-sm" style="width: 150px;">
            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

            <div class="input-group-btn">
              <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            </div>
          </div>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tr>
            <th>Código</th>
            <th>Nombre</th>
            <th>Dni</th>
            <th>Cuil</th>
            <th>Fecha Nac</th>
            <th>Ingreso</th>
            <th>Cargo</th>
            <th>Acciones</th>
          </tr>
	<?php
		for ($i = 0; $i < count($empleados); $i++) {
	?>
          <tr>
            <td><?php echo $empleados[$i]->codigo;  ?></td>
            <td><?php echo $empleados[$i]->nombre.' '.$empleados[$i]->apellido;  ?></td>
            <td><?php echo $empleados[$i]->dni;  ?></td>
            <td><?php echo $empleados[$i]->cuil;  ?></td>
            <td><?php echo $empleados[$i]->fecha_nacimiento;  ?></td>
            <td><?php echo $empleados[$i]->fecha_ingreso;  ?></td>
            <td><?php echo $empleados[$i]->cargo;  ?></td>
            <td><button onclick="window.location.href='../EmpleadoController/update?idEmpleado=<?php echo $empleados[$i]->codigo;?>'" class="btn btn-warning glyphicon glyphicon-pencil btn-xs"></button>
                  <button onclick="confirmDelete(<?php echo $empleados[$i]->codigo;?>);" class="btn btn-danger glyphicon glyphicon-trash btn-xs" style="margin-left: 10px;"></button>
            </td>
          </tr>
	<?php } ?>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>
<script src="<?php echo base_url('public/js/validateEmpleado.js')?>"></script>