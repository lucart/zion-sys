<?php

class DetalleCompraModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getDetallesCompraByCompra($compraId) {
		$query = $this->db->query("select dc.codigo codigo, ar.nombre articulo, dc.cantidad cantidad  
									from detalle_compra dc, articulo ar  
									where dc.articulo = ar.codigo 
									and dc.compra = $compraId");
		return $query->result();
	}

	public function getArticulo($id) {
		$query = $this->db->query("select codigo, nombre, descripcion, precio, stock, categoria from `ARTICULO` where codigo = $id");
		return $query->result();
	}
	
	public function insertDetalleCompra($data) {
		$this->db->insert('DETALLE_COMPRA',$data);
		return $this->db->insert_id();
	}

	public function deleteArticulo($id) {
      $query = $this->db->query("delete from ARTICULO where codigo = $id");
      
      if($query){
        return true;
      }
      return false;
    }

    public function updateArticulo($nombre, $descripcion, $precio, $stock, $categoria, $idArticulo) {
      $query = $this->db->query("update ARTICULO
                                  set nombre = '$nombre',
                                      descripcion = '$descripcion',
                                      precio = $precio,
                                      stock = $stock,
                                      categoria = $categoria
                                      where codigo = $idArticulo;");
      return true;
    }
}