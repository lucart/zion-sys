<?php

class ProveedorController extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('ProveedorModel');
		$this->load->model('PersonaModel');
		$this->load->model('ProvinciaModel');
		$this->load->model('LocalidadModel');
		$this->load->model('DomicilioModel');
		$this->load->model('CategoriaModel');
		error_reporting(E_ERROR | E_PARSE);
		if(empty($this->session->userdata("logged_in")))
        {
	        redirect('LoginController/index', 'refresh');
	    }
	}

	public function lista(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$data['contenido'] = "proveedor/lista";
		$data['proveedores'] = $this->ProveedorModel->getProveedores();
		$this->load->view("template/template", $data);
	}

	public function create(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$data['contenido'] = "proveedor/create";
		$data['provincias'] = $this->ProvinciaModel->getProvincias();
		$data['localidades'] = $this->LocalidadModel->getLocalidades();
		$data['categorias'] = $this->CategoriaModel->getCategorias();
		$this->load->view("template/template", $data);
	}

	public function update(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$idProveedor = $_GET["idProveedor"];
		$data['contenido'] = "proveedor/update";
		$data['proveedor'] = $this->ProveedorModel->getProveedor($idProveedor);
		$data['provincias'] = $this->ProvinciaModel->getProvincias();
		$data['localidades'] = $this->LocalidadModel->getLocalidades();
		$data['categorias'] = $this->CategoriaModel->getCategorias();
		$data['domicilio'] = $this->DomicilioModel->getDomicilio($data['proveedor'][0]->domicilio);
		$this->load->view("template/template", $data);
	}

	public function eliminarProveedor(){
		$idProveedor = $_GET["idProveedor"];
		$this->ProveedorModel->deleteProveedor($idProveedor);

		redirect(base_url() . 'index.php/ProveedorController/lista');
	}

	
	public function guardarProveedor(){
		$idProveedor = $_POST["idProveedor"];
		$idDomicilio = $_POST["idDomicilio"];
		$idCategoria = $_POST["categoria"];

		$nombre = $_POST["nombre"];
		$razonSocial = $_POST["razon_social"];
		
		//Getting address data by post
		$calle = $_POST["calle"];
		$numero = $_POST["numero"];
		$codigoPostal = $_POST["codigo_postal"];
		$localidad = $_POST["localidad"];
		var_dump($idDomicilio);
		if($idDomicilio != null){
			$this->DomicilioModel->updateDomicilio($idDomicilio, $calle, $numero, $codigoPostal, $localidad);
		}else{			
			$idDomicilio = $this->DomicilioModel->insertDomicilio($calle, $numero, $codigoPostal, $localidad);
		}		
		
		var_dump($idCategoria);
		if($idProveedor == null){
			$this->ProveedorModel->insertProveedor($nombre, $razonSocial, $idCategoria, $idDomicilio[0]->idDomicilio);
		}else{
			$this->ProveedorModel->updateProveedor($idProveedor, $nombre, $razonSocial, $idCategoria, $idDomicilio);
		}

		redirect(base_url() . 'index.php/ProveedorController/lista');
	}
}