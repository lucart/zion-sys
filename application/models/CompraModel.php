<?php

class CompraModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getCompras() {
		$query = $this->db->query("select co.codigo, co.fecha, pr.nombre proveedor, pe.nombre empleado  
									from COMPRA co, PROVEEDOR pr, EMPLEADO em, PERSONA pe  
									where co.proveedor = pr.codigo 
									and co.empleado = em.codigo
									and em.persona = pe.codigo");
		return $query->result();
	}

	public function getArticulo($id) {
		$query = $this->db->query("select codigo, nombre, descripcion, precio, stock, categoria from `ARTICULO` where codigo = $id");
		return $query->result();
	}
	
	public function insertCompra($data) {
		$this->db->insert('COMPRA',$data);
		return $this->db->insert_id();
	}

	public function deleteArticulo($id) {
      $query = $this->db->query("delete from ARTICULO where codigo = $id");
      
      if($query){
        return true;
      }
      return false;
    }

    public function updateArticulo($nombre, $descripcion, $precio, $stock, $categoria, $idArticulo) {
      $query = $this->db->query("update ARTICULO
                                  set nombre = '$nombre',
                                      descripcion = '$descripcion',
                                      precio = $precio,
                                      stock = $stock,
                                      categoria = $categoria
                                      where codigo = $idArticulo;");
      return true;
    }
}