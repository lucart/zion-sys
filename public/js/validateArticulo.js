function validate(id) {
  	var campos = "";
  	
  	if(!$("#nombre").val())
  		campos = campos + "Nombre \n";
    if(!$("#descripcion").val())
      campos = campos + "Descripcion \n";
    if(!$("#precio").val())
      campos = campos + "Precio \n";
    if(!$("#stock").val())
      campos = campos + "Stock \n";
  	if($("#categoria").val() == 0)
  		campos = campos + "Categoria \n";
  	
  	if(campos.length === 0){
  		$('form#guardarArticuloForm').submit();
  	}else{
  		swal("Completar los siguientes campos: ", campos);
  	}
}

function confirmDelete(id) {
    swal({
        title: "Eliminar Articulo",
        text: "¿Está seguro que desea eliminar este Articulo?",
        type: "error",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Eliminar!',
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true
      },
      function(isConfirm){
        if (isConfirm) {
              window.location.href = "../ArticuloController/eliminarArticulo?idArticulo=" + id;
        }
    });
}