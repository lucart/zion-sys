function validate(id) {
  	var campos = "";
  	
  	if(!$("#nombre").val())
  		campos = campos + "Nombre \n";
  	
  	if(campos.length === 0){
  		$('form#guardarCargoForm').submit();
  	}else{
  		swal("Completar los siguientes campos: ", campos);
  	}
}

function confirmDelete(id) {
    swal({
        title: "Eliminar Cargo",
        text: "¿Está seguro que desea eliminar esta cargo?",
        type: "error",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Eliminar!',
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true
      },
      function(isConfirm){
        if (isConfirm) {
              window.location.href = "../CargoController/eliminarCargo?idCargo=" + id;
        }
    });
}