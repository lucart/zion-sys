<?php

class CategoriaController extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('CategoriaModel');
		error_reporting(E_ERROR | E_PARSE);
		if(empty($this->session->userdata("logged_in")))
	    {
	      redirect('LoginController/index', 'refresh');
	    }
	}

	public function lista(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$data['contenido'] = "categoria/lista";
		$data['categorias'] = $this->CategoriaModel->getCategorias();
		$this->load->view("template/template", $data);
	}

	public function create(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$data['contenido'] = "categoria/create";
		$this->load->view("template/template", $data);
	}
	
	public function guardarCategoria(){
		$nombre = $_POST["nombre"];
		$descripcion = $_POST["descripcion"];
		$idCategoria = $_POST["idCategoria"];

		if($idCategoria != null){
			$this->CategoriaModel->updateCategoria($nombre, $descripcion, $idCategoria);
		}else{
			$this->CategoriaModel->insertCategoria($nombre, $descripcion);
		}
				
		redirect(base_url() . 'index.php/CategoriaController/lista');
	}

	public function eliminarCategoria(){
		$idCategoria = $_GET["idCategoria"];
		$this->CategoriaModel->deleteCategoria($idCategoria);

		redirect(base_url() . 'index.php/CategoriaController/lista');
	}

	public function update(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$idCategoria = $_GET["idCategoria"];
		$data['contenido'] = "categoria/update";
		$data['categoria'] = $this->CategoriaModel->getCategoria($idCategoria);
		$this->load->view("template/template", $data);
	}

}