<?php

class CargoModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getCargos() {
		$query = $this->db->query("select ca.codigo, ca.nombre, ca.descripcion 
									from `CARGO` ca");
		return $query->result();
	}

	public function getCargo($id) {
		$query = $this->db->query("select codigo, nombre, descripcion from `CARGO` where codigo = $id");
		return $query->result();
	}
	
	public function insertCargo($nombre, $descripcion) {
		$query = $this->db->query("INSERT INTO `CARGO` ( `nombre`, `descripcion`) 
									VALUES ('$nombre','$descripcion');");
		return true;
	}

	public function deleteCargo($id) {
      $query = $this->db->query("delete from CARGO where codigo = $id");
      
      if($query){
        return true;
      }
      return false;
    }

    public function updateCargo($nombre, $descripcion, $idCargo) {
      $query = $this->db->query("update CARGO
                                  set nombre = '$nombre',
                                      descripcion = '$descripcion'
                                      where codigo = $idCargo;");
      return true;
    }
}