<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Editar Usuario</h3>
  </div>
  <div class="box-body">
    <form role="form" id="guardarUsuarioForm" action="../UsuarioController/guardarUsuario" method="post" >
      <input type="hidden" name="idUsuario" value="<?php echo $usuario[0]->codigo?>">
      <table class="table table-hover">
        <h3>Usuario</h3>
        <tr width="50%">
          <td width="25%">
            <div class="form-group">
              <label>Nombre</label>
              <input type="text" id="nombre" name="nombre" value="<?php echo $usuario[0]->nombre?>" class="form-control" placeholder="Ingrese el Nombre del Usuario">
            </div>
          </td>
          <td width="5%"></td>          
            <td width="25%">
              <div class="form-group">
                <label>Apellido</label>
                <input type="text" id="apellido" name="apellido" value="<?php echo $usuario[0]->apellido?>" class="form-control" placeholder="Ingrese el Apellido del Usuario">
              </div>
          </td> 
          <td></td>         
          <td></td>         
          <td></td>
        </tr>
        <tr>
          <td width="25%">
            <div class="form-group">
              <label>Username</label>
              <input type="text" id="username" name="username" value="<?php echo $usuario[0]->username?>" class="form-control" placeholder="Ingrese el Username del Usuario">
            </div>
          </td>
          <td width="5%"></td>          
            <td width="25%">
              <div class="form-group">
                <label>Password</label>
                <input type="password" id="password" name="password" value="<?php echo $usuario[0]->password?>" class="form-control" placeholder="Ingrese el Password del Usuario">
              </div>
          </td> 
          <td></td>         
          <td></td>         
          <td></td>
        </tr>
        <tr>
          <td width="25%">
            <div class="form-group">
              <label>Email</label>
              <input type="text" id="email" name="email" value="<?php echo $usuario[0]->email?>" class="form-control" placeholder="Ingrese el Email del Usuario">
            </div>
          </td>
          <td width="5%"></td>          
            <td width="25%">
              <div class="form-group">
              <label>Permiso</label>
              <select id="permiso" name="permiso" class="form-control" value="2">
                <option  value="0">Seleccione...</option>
              <?php
                for ($i = 0; $i < count($permisos); $i++) {
                  if($permisos[$i]->codigo == $usuario[0]->codigo_permiso) {
              ?>
                <option selected value="<?php echo $permisos[$i]->codigo?>"><?php echo $permisos[$i]->nombre?></option>
              <?php } else {?>
                <option value="<?php echo $permisos[$i]->codigo?>"><?php echo $permisos[$i]->nombre?></option>
               <?php } } ?>
              </select>
            </div>
          </td> 
          <td></td>         
          <td></td>         
          <td></td>
        </tr>
      </table>

      <div class="box-footer">
        <button type="button" onClick="validate();" class="btn btn-primary">Guardar</button>
      </div>
    </form>
  </div>
</div>
<script src="<?php echo base_url('public/js/validateUsuario.js')?>"></script>