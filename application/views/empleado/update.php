<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Editar Empleado/a</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <form id="guardarEmpleadoForm" role="form" action="../EmpleadoController/guardarEmpleado" method="post" >
      <input type="hidden" name="idEmpleado" value="<?php echo $empleado[0]->codigo?>">
      <input type="hidden" name="idPersona" value="<?php echo $empleado[0]->persona?>">
      <input type="hidden" name="idDomicilio" value="<?php echo $empleado[0]->dolmicilio?>">
	  <table class="table table-hover">
		  <h3>Persona</h3>
		  	<tr>
              <td width="25%"><!-- text input -->
				  <div class="form-group">
					<label>Nombre</label>
					<input type="text" id="nombre" name="nombre" value="<?php echo $empleado[0]->nombre?>" class="form-control" placeholder="Ingrese el Nombre del Empleado">
				  </div>
			  </td>
			  <td width="5%"></td>				  
              <td width="25%">
				<!-- text input -->
				  <div class="form-group">
					<label>Apellido</label>
					<input type="text" id="apellido" name="apellido" value="<?php echo $empleado[0]->apellido?>" class="form-control" placeholder="Ingrese el Apellido del Empleado">
				  </div>
			  </td>	
			  <td width="45%"></td>	
            </tr>
            <tr>
              <td width="25%">
				  <!-- text input -->
				  <div class="form-group">
					<label>Dni</label>
					<input type="text" id="dni" name="dni" class="form-control" value="<?php echo $empleado[0]->dni?>" placeholder="Ingrese el Dni del Empleado">
				  </div>
			  </td>
			  <td width="5%"></td>		
              <td width="25%"> 
				  <!-- date input -->
				  <div class="form-group">
					<label>Fecha de Nacimiento</label>
					<input type="date" id="fecha_nacimiento" name="fecha_nacimiento" value="<?php echo $empleado[0]->fecha_nacimiento?>" class="form-control" placeholder="Ingrese la Fecha de Nacimiento">
				  </div>
				</td>
			  <td width="45%"></td>	
            </tr>
          </table>
		  <div class="panel-group">
			  <div class="panel panel-default">
				<div class="panel-heading">
				  <h4 class="panel-title">
					<a data-toggle="collapse" href="#collapse1">Dirección</a>
				  </h4>
				</div>
				<div id="collapse1" class="panel-collapse collapse">					  
					<table class="table table-hover">
						<tr>                  
						  <td width="25%">
							<!-- select input -->
							  <div class="form-group">
								<label>Provincia</label>
								<select id="provincia" name="provincia" class="form-control" onchange="cargarLocalidades();" >  
				                    <option  value="0">Seleccione...</option>    
				                    <?php
				                      for ($i = 0; $i < count($provincias); $i++) {
				                        if($provincias[$i]->codigo == $empleado[0]->provincia) {
				                    ?>
				                      <option selected value="<?php echo $provincias[$i]->codigo?>"><?php echo $provincias[$i]->nombre?></option>
				                    <?php } else {?>
				                      <option value="<?php echo $provincias[$i]->codigo?>"><?php echo $provincias[$i]->nombre?></option>
				                     <?php } } ?>  
				                  </select>
							  </div>
						  </td>
						  <td width="5%"></td>
						  <td width="25%"> 
							  <!-- select input -->
							  <div class="form-group">
								<label>Localidad</label>
								<select id="localidad" name="localidad" class="form-control" ></select>
							  </div>
							</td>
						  <td width="45%"></td>
						</tr>
						<tr>
						  <td width="25%"><!-- text input -->
							  <div class="form-group">
								<label>Calle</label>
								<input type="text" id="calle" name="calle" value="<?php echo $domicilio[0]->calle?>" class="form-control" placeholder="Ingrese la calle">
							  </div>
						  </td>
						  <td width="5%"></td>
						  <td width="25%">
							<!-- text input -->
							  <div class="form-group">
								<label>Número</label>
								<input type="number" id="numero" name="numero" value="<?php echo $domicilio[0]->numero?>" class="form-control" placeholder="Ingrese el Numero">
							  </div>
						  </td>
						  <td width="45%"></td>
						</tr>
						  <td width="25%"> 
							  <!-- select input -->
							  <div class="form-group">
								<label>Código Postal</label>									
								<input type="text" id="codigo_postal" name="codigo_postal" value="<?php echo $domicilio[0]->codigo_postal?>" class="form-control" placeholder="Ingrese el Código Postal del Empleado">
							  </div>
							</td>
							<td width="5%"></td>
							<td width="25%"></td>
						  <td width="45%"></td>
						</tr>
					</table>	
				</div>
			  </div>
			</div>	
			<table class="table table-hover">
			  <h3>Empleado</h3>
                <tr>
                  <td width="25%">
					<!-- text input -->
					  <div class="form-group">
						<label>Cuil</label>
						<input type="number" id="cuil" name="cuil" value="<?php echo $empleado[0]->cuil?>" class="form-control" placeholder="Ingrese el Cuil">
					  </div>
				  </td>
				  <td width="5%"></td>
				  <td width="25%"> 
					  <!-- date input -->
					  <div class="form-group">
						<label>Fecha de Ingreso</label>
						<input type="date" id="fecha_ingreso" value="<?php echo $empleado[0]->fecha_ingreso?>" name="fecha_ingreso" class="form-control" placeholder="Ingrese la Fecha de Ingreso">
					  </div>
					</td>
				  <td width="45%"></td>		
                </tr>
                <tr>
				  <td width="25%">
				  	<div class="form-group">
						<label>Cargo</label>
						<select id="cargo" name="cargo" class="form-control">  
		                    <option  value="0">Seleccione...</option>    
		                    <?php
		                      for ($i = 0; $i < count($cargos); $i++) {
		                        if($cargos[$i]->codigo == $empleado[0]->cargo) {
		                    ?>
		                      <option selected value="<?php echo $cargos[$i]->codigo?>"><?php echo $cargos[$i]->nombre?></option>
		                    <?php } else {?>
		                      <option value="<?php echo $cargos[$i]->codigo?>"><?php echo $cargos[$i]->nombre?></option>
		                     <?php } } ?>  
		                  </select>
					  </div>
				  </td>
				  <td width="5%"></td>
				  <td width="25%"></td>
				  <td width="45%"></td> 		
                </tr>
              </table>	
        <div class="box-footer">
          <button type="button" onClick="validate();" class="btn btn-primary">Guardar</button>
        </div>
    </form>
  </div>
</div>
<script src="<?php echo base_url('public/js/validateEmpleado.js')?>"></script>
<script>
	document.getElementById("provincia").value=<?php echo $empleado[0]->provincia;?>;
	cargarLocalidades( );
	document.getElementById("localidad").value=<?php echo $empleado[0]->localidad;?>;
	function cargarLocalidades() {
		var provincia=document.getElementById("provincia").value;
		var localidad=document.getElementById("localidad");
		localidad.options.length =0;
		
		<?php for($i = 0; $i < count($localidades); $i++){?>
			if(<?php echo $localidades[$i]->provincia; ?> == provincia){				
				var option = document.createElement("option");
				option.value = <?php echo $localidades[$i]->codigo; ?>;
				option.text = <?php echo "'".$localidades[$i]->nombre."'"; ?>;
				localidad.add(option);
			}
			
		<?php }?>
	  }
</script>


