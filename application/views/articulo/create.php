<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Crear un Nuevo Articulo</h3>
  </div>
  <div class="box-body">
    <form id="guardarArticuloForm" role="form" action="../ArticuloController/guardarArticulo" method="post" >		
	  <table class="table table-hover">
		<tr>
		  <td colspan="2" width="20%">		  
			  <div class="form-group">
				<label>Nombre</label>
				<input type="text" id="nombre" name="nombre" class="form-control" placeholder="Ingrese el Nombre del Articulo">
			  </div>
		  </td>
		  <td width="20%"></td>
		  <td width="60%"></td>
		</tr>
		<tr>
		  <td colspan="2" width="20%">
			  <div class="form-group">
				<label>Categoría</label>
				<select id="categoria" name="categoria" class="form-control" >	
					<option  value="0">Seleccione...</option>						
					<?php
						for ($i = 0; $i < count($categorias); $i++) {
					?>
					<option  value="<?php echo $categorias[$i]->codigo?>"><?php echo $categorias[$i]->nombre?></option>
					<?php } ?>
				</select>
			  </div>
		  </td>
		  <td width="20%"></td>
		  <td width="60%"></td>
		</tr>
		<tr>
		  <td colspan="2">
			  <div class="form-group">
				<label>Descripcion</label>
				<textarea class="form-control" id="descripcion" name="descripcion" rows="5" placeholder="Ingrese la descripcion"></textarea>
			  </div>
		  </td>
		</tr>
		<tr>
		  <td width="20%">		  
			  <div class="form-group">
				<label>Precio</label>
				<input type="number" id="precio" name="precio" class="form-control" placeholder="Ingrese el Precio del Articulo">
			  </div>
		  </td>
		  <td width="20%">		  
			  <div class="form-group">
				<label>Stock</label>
				<input type="number" id="stock" name="stock" class="form-control" placeholder="Ingrese el Stock del Articulo">
			  </div>
		  </td>
		  <td width="60%"></td>
		</tr>
	  </table>
      <div class="box-footer">
        <button type="button" onClick="validate();" class="btn btn-primary">Guardar</button>
      </div>
    </form>
  </div>
</div>
<script src="<?php echo base_url('public/js/validateArticulo.js')?>"></script>