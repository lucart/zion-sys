function validate(id) {
  	var campos = "";
  	
  	if(!$("#nombre").val())
  		campos = campos + "Nombre \n";
  	if(!$("#apellido").val())
  		campos = campos + "Apellido \n";
  	if(!$("#username").val())
  		campos = campos + "Username \n";
  	if(!$("#password").val())
  		campos = campos + "Password \n";
  	if(!$("#email").val())
  		campos = campos + "Email \n";
  	if($("#permiso").val() == 0)
  		campos = campos + "Permiso \n";
  	
  	if(campos.length === 0){
  		$('form#guardarUsuarioForm').submit();
  	}else{
  		swal("Completar los siguientes campos: ", campos);
  	}
  }

function confirmDelete(id) {
  swal({
      title: "Eliminar Usuario",
      text: "¿Está seguro que desea eliminar este usuario?",
      type: "error",
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Eliminar!',
      cancelButtonText: "Cancelar",
      closeOnConfirm: false,
      closeOnCancel: true
    },
    function(isConfirm){
      if (isConfirm) {
            window.location.href = "../UsuarioController/eliminarUsuario?idUsuario=" + id;
      }
  });
}