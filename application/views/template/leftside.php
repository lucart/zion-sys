<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url('public/img/default_user.png')?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $nombre." ".$apellido; ?></p>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MENU</li>
        <!-- USUARIOS -->
        <?php if($permiso == 1) { ?>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Usuarios</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('index.php/UsuarioController/create')?>"><i class="fa fa-circle-o"></i> Nuevo Usuario</a></li>
            <li><a href="<?php echo base_url('index.php/UsuarioController/lista')?>"><i class="fa fa-circle-o"></i> Listado de Usuarios</a></li>
          </ul>
        </li>
        <?php } ?>
        <!-- Articulo -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Articulos</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('index.php/ArticuloController/create')?>"><i class="fa fa-circle-o"></i> Nuevo Articulo</a></li>
            <li><a href="<?php echo base_url('index.php/ArticuloController/lista')?>"><i class="fa fa-circle-o"></i> Listado de Articulos</a></li>
			<li><a href="<?php echo base_url('index.php/CategoriaController/create')?>"><i class="fa fa-circle-o"></i> Nueva Categoria</a></li>
            <li><a href="<?php echo base_url('index.php/CategoriaController/lista')?>"><i class="fa fa-circle-o"></i> Listado de Categorias</a></li>
          </ul>
        </li>
        <!-- Empleado -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Empleados</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('index.php/EmpleadoController/create')?>"><i class="fa fa-circle-o"></i> Nuevo Empleado</a></li>
            <li><a href="<?php echo base_url('index.php/EmpleadoController/lista')?>"><i class="fa fa-circle-o"></i> Listado de Empleados</a></li>
			<li><a href="<?php echo base_url('index.php/CargoController/create')?>"><i class="fa fa-circle-o"></i> Nuevo Cargo</a></li>
            <li><a href="<?php echo base_url('index.php/CargoController/lista')?>"><i class="fa fa-circle-o"></i> Listado de Cargos</a></li>
          </ul>
        </li>
        <!-- Proveedor -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Proveedores</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('index.php/ProveedorController/create')?>"><i class="fa fa-circle-o"></i> Nuevo Proveedor</a></li>
            <li><a href="<?php echo base_url('index.php/ProveedorController/lista')?>"><i class="fa fa-circle-o"></i> Listado de Proveedores</a></li>
          </ul>
        </li>		
        <!-- Compra -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Compras</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('index.php/CompraController/create')?>"><i class="fa fa-circle-o"></i> Nueva Compra</a></li>
            <li><a href="<?php echo base_url('index.php/CompraController/lista')?>"><i class="fa fa-circle-o"></i> Listado de Compras</a></li>
          </ul>
        </li>			
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>