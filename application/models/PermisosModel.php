<?php

class PermisosModel extends CI_Model {

    function __construct() {
      parent::__construct();
      $this->load->database();
    }

    public function getPermisos() {
      $query = $this->db->query("select codigo, nombre, descripcion from PERMISOS");
      return $query->result();
    }
}
?>