function validate(id) {
  	var campos = "";
  	
  	if(!$("#nombre").val())
  		campos = campos + "Nombre \n";
    if(!$("#razon_social").val())
      campos = campos + "Razon Social \n";
    if(!$("#calle").val())
      campos = campos + "Calle \n";
    if(!$("#numero").val())
      campos = campos + "Numero \n";
    if(!$("#codigo_postal").val())
      campos = campos + "Codigo Postal \n";
  	if($("#provincia").val() == 0)
  		campos = campos + "Provincia \n";
    if($("#localidad").val() == 0)
      campos = campos + "Localidad \n";
    if($("#categoria").val() == 0)
      campos = campos + "Categoria \n";
  	
  	if(campos.length === 0){
  		$('form#guardarProveedorForm').submit();
  	}else{
  		swal("Completar los siguientes campos: ", campos);
  	}
}

function confirmDelete(id) {
    swal({
        title: "Eliminar Proveedor",
        text: "¿Está seguro que desea eliminar este proveedor?",
        type: "error",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Eliminar!',
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true
      },
      function(isConfirm){
        if (isConfirm) {
              window.location.href = "../ProveedorController/eliminarProveedor?idProveedor=" + id;
        }
    });
}