<?php

class ProveedorModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getProveedores() {
		$query = $this->db->query("select pro.codigo, pro.nombre, pro.razon_social, lo.nombre localidad, 
									ca.nombre categoria, pr.nombre provincia
									from PROVEEDOR pro, DOMICILIO do, CATEGORIA ca, LOCALIDAD lo, 
										 PROVINCIA pr 
									where pro.categoria = ca.codigo 
									and pro.domicilio = do.codigo 
									and do.localidad = lo.codigo 
									and lo.provincia = pr.codigo");
		return $query->result();
	}

	public function getProveedor($id) {
		$query = $this->db->query("select pro.codigo, pro.nombre, pro.razon_social, lo.codigo localidad, 
									ca.codigo categoria, pr.codigo provincia, pro.domicilio 
									from PROVEEDOR pro, DOMICILIO do, CATEGORIA ca, LOCALIDAD lo, 
										 PROVINCIA pr 
									where pro.categoria = ca.codigo 
									and pro.domicilio = do.codigo 
									and do.localidad = lo.codigo 
									and lo.provincia = pr.codigo
									and pro.codigo = $id");
		return $query->result();
	}
	
	public function insertProveedor($nombre, $razonSocial, $idCategoria, $idDomicilio) {
		$query = $this->db->query("INSERT INTO `PROVEEDOR` ( `nombre`, `razon_social`, `categoria`, `domicilio`) 
									VALUES ('$nombre','$razonSocial','$idCategoria','$idDomicilio');");
		return true;
	}

	public function updateProveedor($idProveedor, $nombre, $razonSocial, $idCategoria, $idDomicilio) {
      $query = $this->db->query("update PROVEEDOR
                                  set nombre = '$nombre',
                                      razon_social = '$razonSocial',
                                      domicilio = $idDomicilio, 
                                      categoria = $idCategoria
                                      where codigo = $idProveedor;");
      return true;
    }
	
	public function deleteProveedor($id) {
      $query = $this->db->query("delete from PROVEEDOR where codigo = $id");
      
      if($query){
        return true;
      }
      return false;
    }
}