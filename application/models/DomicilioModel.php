<?php

class DomicilioModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getDomicilios() {
		$query = $this->db->query("select * from DOMICILIO");
		return $query->result();
	}

	public function getDomicilio($id) {
		$query = $this->db->query("select * from DOMICILIO where codigo = $id");
		return $query->result();
	}
	
	public function insertDomicilio($calle, $numero, $codigoPostal, $localidad) {
		$queryIns = $this->db->query("INSERT INTO `DOMICILIO` ( `calle`, `numero`,`codigo_postal`,`localidad`) 
									VALUES ('$calle','$numero',$codigoPostal, $localidad);");								
		$query = $this->db->query("SELECT MAX(codigo) as idDomicilio from DOMICILIO;");
		
		return $query->result();
	}
	
	public function updateDomicilio($idDomicilio, $calle, $numero, $codigoPostal, $localidad) {
      $query = $this->db->query("update DOMICILIO
                                  set calle = '$calle',
                                      numero = $numero,
                                      localidad = $localidad,
                                      codigo_postal = '$codigoPostal'
                                      where codigo = $idDomicilio;");
      $query = $this->db->query("SELECT MAX(codigo) as idDomicilio from DOMICILIO;");
		
		return $query->result();
	}
}