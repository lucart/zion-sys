<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Editar Proveedor</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <form id="guardarProveedorForm" role="form" action="../ProveedorController/guardarProveedor" method="post" >
      <input type="hidden" name="idProveedor" value="<?php echo $proveedor[0]->codigo?>">
      <input type="hidden" name="idCategoria" value="<?php echo $proveedor[0]->categoria?>">
      <input type="hidden" name="idDomicilio" value="<?php echo $proveedor[0]->domicilio?>">
	  <table class="table table-hover">
		  <h3>Proveedor</h3>
		  	<tr>
              <td width="25%"><!-- text input -->
				  <div class="form-group">
					<label>Nombre</label>
					<input type="text" id="nombre" name="nombre" value="<?php echo $proveedor[0]->nombre?>" class="form-control" placeholder="Ingrese el Nombre del Proveedor">
				  </div>
			  </td>
			  <td width="5%"></td>				  
              <td width="25%">
				<!-- text input -->
				  <div class="form-group">
					<label>Razon Social</label>
					<input type="text" id="razon_social" name="razon_social" value="<?php echo $proveedor[0]->razon_social?>" class="form-control" placeholder="Ingrese la Razon Social del Proveedor">
				  </div>
			  </td>	
			  <td width="45%"></td>	
            </tr>
          </table>
		  <div class="panel-group">
			  <div class="panel panel-default">
				<div class="panel-heading">
				  <h4 class="panel-title">
					<a data-toggle="collapse" href="#collapse1">Dirección</a>
				  </h4>
				</div>
				<div id="collapse1" class="panel-collapse collapse">					  
					<table class="table table-hover">
						<tr>                  
						  <td width="25%">
							<!-- select input -->
							  <div class="form-group">
								<label>Provincia</label>
								<select id="provincia" name="provincia" class="form-control" onchange="cargarLocalidades();" >  
				                    <option  value="0">Seleccione...</option>    
				                    <?php
				                      for ($i = 0; $i < count($provincias); $i++) {
				                        if($provincias[$i]->codigo == $proveedor[0]->provincia) {
				                    ?>
				                      <option selected value="<?php echo $provincias[$i]->codigo?>"><?php echo $provincias[$i]->nombre?></option>
				                    <?php } else {?>
				                      <option value="<?php echo $provincias[$i]->codigo?>"><?php echo $provincias[$i]->nombre?></option>
				                     <?php } } ?>  
				                  </select>
							  </div>
						  </td>
						  <td width="5%"></td>
						  <td width="25%"> 
							  <!-- select input -->
							  <div class="form-group">
								<label>Localidad</label>
								<select id="localidad" name="localidad" class="form-control" ></select>
							  </div>
							</td>
						  <td width="45%"></td>
						</tr>
						<tr>
						  <td width="25%"><!-- text input -->
							  <div class="form-group">
								<label>Calle</label>
								<input type="text" id="calle" name="calle" value="<?php echo $domicilio[0]->calle?>" class="form-control" placeholder="Ingrese la calle">
							  </div>
						  </td>
						  <td width="5%"></td>
						  <td width="25%">
							<!-- text input -->
							  <div class="form-group">
								<label>Número</label>
								<input type="number" id="numero" name="numero" value="<?php echo $domicilio[0]->numero?>" class="form-control" placeholder="Ingrese el Numero">
							  </div>
						  </td>
						  <td width="45%"></td>
						</tr>
						  <td width="25%"> 
							  <!-- select input -->
							  <div class="form-group">
								<label>Código Postal</label>									
								<input type="text" id="codigo_postal" name="codigo_postal" value="<?php echo $domicilio[0]->codigo_postal?>" class="form-control" placeholder="Ingrese el Código Postal del Proveedor">
							  </div>
							</td>
							<td width="5%"></td>
							<td width="25%"></td>
						  <td width="45%"></td>
						</tr>
					</table>	
				</div>
			  </div>
			</div>	
			<table class="table table-hover">
                <tr>
				  <td width="25%">
				  	<div class="form-group">
						<label>Categoria</label>
						<select id="categoria" name="categoria" class="form-control">  
		                    <option  value="0">Seleccione...</option>    
		                    <?php
		                      for ($i = 0; $i < count($categorias); $i++) {
		                        if($categorias[$i]->codigo == $proveedor[0]->categoria) {
		                    ?>
		                      <option selected value="<?php echo $categorias[$i]->codigo?>"><?php echo $categorias[$i]->nombre?></option>
		                    <?php } else {?>
		                      <option value="<?php echo $categorias[$i]->codigo?>"><?php echo $categorias[$i]->nombre?></option>
		                     <?php } } ?>  
		                  </select>
					  </div>
				  </td>
				  <td width="5%"></td>
				  <td width="25%"></td>
				  <td width="45%"></td> 		
                </tr>
              </table>	
        <div class="box-footer">
          <button type="button" onClick="validate();" class="btn btn-primary">Guardar</button>
        </div>
    </form>
  </div>
</div>
<script src="<?php echo base_url('public/js/validateProveedor.js')?>"></script>
<script>
	document.getElementById("provincia").value=<?php echo $proveedor[0]->provincia;?>;
	cargarLocalidades( );
	document.getElementById("localidad").value=<?php echo $proveedor[0]->localidad;?>;
	function cargarLocalidades() {
		var provincia=document.getElementById("provincia").value;
		var localidad=document.getElementById("localidad");
		localidad.options.length =0;
		
		<?php for($i = 0; $i < count($localidades); $i++){?>
			if(<?php echo $localidades[$i]->provincia; ?> == provincia){				
				var option = document.createElement("option");
				option.value = <?php echo $localidades[$i]->codigo; ?>;
				option.text = <?php echo "'".$localidades[$i]->nombre."'"; ?>;
				localidad.add(option);
			}
			
		<?php }?>
	  }
</script>


