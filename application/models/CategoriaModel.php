<?php

class CategoriaModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getCategorias() {
		$query = $this->db->query("select ca.codigo, ca.nombre, ca.descripcion 
									from `CATEGORIA` ca");
		return $query->result();
	}

	public function getCategoria($id) {
		$query = $this->db->query("select codigo, nombre, descripcion from `CATEGORIA` where codigo = $id");
		return $query->result();
	}
	
	public function insertCategoria($nombre, $descripcion) {
		$query = $this->db->query("INSERT INTO `CATEGORIA` ( `nombre`, `descripcion`) 
									VALUES ('$nombre','$descripcion');");
		return true;
	}

	public function deleteCategoria($id) {
      $query = $this->db->query("delete from CATEGORIA where codigo = $id");
      
      if($query){
        return true;
      }
      return false;
    }

    public function updateCategoria($nombre, $descripcion, $idCategoria) {
      $query = $this->db->query("update CATEGORIA
                                  set nombre = '$nombre',
                                      descripcion = '$descripcion'
                                      where codigo = $idCategoria;");
      return true;
    }
}