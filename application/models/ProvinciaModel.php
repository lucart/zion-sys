<?php

class ProvinciaModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getProvincias() {
		$query = $this->db->query("select po.codigo, po.nombre
									from PROVINCIA po");
		return $query->result();
	}

	public function getProvincia($id) {
		$query = $this->db->query("select po.codigo, po.nombre
									from PROVINCIA po
									where po.codigo = $id ");
		return $query->result();
	}
	
	
	
	
}