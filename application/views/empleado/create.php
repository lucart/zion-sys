<div class="box box-warning">
  <div class="box-header with-border">
    <h3 class="box-title">Crear un Nuevo Empleado</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <form id="guardarEmpleadoForm" role="form" action="../EmpleadoController/guardarEmpleado" method="post" >		
              <table class="table table-hover">
			  <h3>Persona</h3>
                <tr>
                  <td width="25%"><!-- text input -->
					  <div class="form-group">
						<label>Nombre</label>
						<input type="text" id="nombre" name="nombre" class="form-control" placeholder="Ingrese el Nombre del Empleado">
					  </div>
				  </td>
				  <td width="5%"></td>				  
                  <td width="25%">
					<!-- text input -->
					  <div class="form-group">
						<label>Apellido</label>
						<input type="text" id="apellido" name="apellido" class="form-control" placeholder="Ingrese el Apellido del Empleado">
					  </div>
				  </td>	
				  <td width="45%"></td>	
                </tr>
                <tr>
                  <td width="25%">
					  <!-- text input -->
					  <div class="form-group">
						<label>Dni</label>
						<input type="text" id="dni" name="dni" class="form-control" placeholder="Ingrese el Dni del Empleado">
					  </div>
				  </td>
				  <td width="5%"></td>		
                  <td width="25%"> 
					  <!-- date input -->
					  <div class="form-group">
						<label>Fecha de Nacimiento</label>
						<input type="date" id="fecha_nacimiento" name="fecha_nacimiento" class="form-control" placeholder="Ingrese la Fecha de Nacimiento">
					  </div>
					</td>
				  <td width="45%"></td>	
                </tr>
              </table>
			  <div class="panel-group">
				  <div class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" href="#collapse1">Dirección</a>
					  </h4>
					</div>
					<div id="collapse1" class="panel-collapse collapse">					  
						<table class="table table-hover">
							<tr>                  
							  <td width="25%">
								<!-- select input -->
								  <div class="form-group">
									<label>Provincia</label>
									<select id="provincia" name="provincia" class="form-control" onchange="cargarLocalidades();" >							
										<?php
											for ($i = 0; $i < count($provincias); $i++) {
											

										?>
										<option  value="<?php echo $provincias[$i]->codigo?>"><?php echo $provincias[$i]->nombre?></option>
										
										<?php } ?>
									</select>
								  </div>
							  </td>
							  <td width="5%"></td>
							  <td width="25%"> 
								  <!-- select input -->
								  <div class="form-group">
									<label>Localidad</label>
									<select id="localidad" name="localidad" class="form-control" ></select>
								  </div>
								</td>
							  <td width="45%"></td>
							</tr>
							<tr>
							  <td width="25%"><!-- text input -->
								  <div class="form-group">
									<label>Calle</label>
									<input type="text" id="calle" name="calle" class="form-control" placeholder="Ingrese la calle">
								  </div>
							  </td>
							  <td width="5%"></td>
							  <td width="25%">
								<!-- text input -->
								  <div class="form-group">
									<label>Número</label>
									<input type="number" id="numero" name="numero" class="form-control" placeholder="Ingrese el Numero">
								  </div>
							  </td>
							  <td width="45%"></td>
							</tr>
							  <td width="25%"> 
								  <!-- select input -->
								  <div class="form-group">
									<label>Código Postal</label>									
									<input type="text" id="codigo_postal" name="codigo_postal" class="form-control" placeholder="Ingrese el Código Postal del Empleado">
								  </div>
								</td>
								<td width="5%"></td>
								<td width="25%"></td>
							  <td width="45%"></td>
							</tr>
						</table>	
					</div>
				  </div>
				</div>	
			  <table class="table table-hover">
			  <h3>Empleado</h3>
                <tr>
                  <td width="25%">
					<!-- text input -->
					  <div class="form-group">
						<label>Cuil</label>
						<input type="number" id="cuil" name="cuil" class="form-control" placeholder="Ingrese el Cuil">
					  </div>
				  </td>
				  <td width="5%"></td>
				  <td width="25%"> 
					  <!-- date input -->
					  <div class="form-group">
						<label>Fecha de Ingreso</label>
						<input type="date" id="fecha_ingreso" name="fecha_ingreso" class="form-control" placeholder="Ingrese la Fecha de Ingreso">
					  </div>
					</td>
				  <td width="45%"></td>		
                </tr>
                <tr>
				  <td width="25%">
				  	<div class="form-group">
						<label>Cargo</label>
						<select id="cargo" name="cargo" class="form-control">							
							<?php
								for ($i = 0; $i < count($cargos); $i++) {
							?>
							<option  value="<?php echo $cargos[$i]->codigo?>"><?php echo $cargos[$i]->nombre?></option>
							<?php } ?>
						</select>
					  </div>
				  </td>
				  <td width="5%"></td>
				  <td width="25%"></td>
				  <td width="45%"></td> 		
                </tr>
              </table>
      <div class="box-footer">
        <button type="button" onClick="validate();" class="btn btn-primary">Guardar</button>
      </div>

      
    </form>
  </div>
  <!-- /.box-body -->
</div>
<script src="<?php echo base_url('public/js/validateEmpleado.js')?>"></script>
<script>
	cargarLocalidades( );
	
	function cargarLocalidades( ){
		var provincia=document.getElementById("provincia").value;
		var localidad=document.getElementById("localidad");
		localidad.options.length =0;
		
		<?php for($i = 0; $i < count($localidades); $i++){?>
			if(<?php echo $localidades[$i]->provincia; ?> == provincia){				
				var option = document.createElement("option");
				option.value = <?php echo $localidades[$i]->codigo; ?>;
				option.text = <?php echo "'".$localidades[$i]->nombre."'"; ?>;
				localidad.add(option);
			}			
		<?php }?>
	}	
</script>