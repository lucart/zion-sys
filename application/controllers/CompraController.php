<?php

class CompraController extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('ProveedorModel');
		$this->load->model('PersonaModel');
		$this->load->model('EmpleadoModel');
		$this->load->model('ArticuloModel');
		$this->load->model('CompraModel');
		$this->load->model('DetalleCompraModel');
		error_reporting(E_ERROR | E_PARSE);
		if(empty($this->session->userdata("logged_in")))
        {
	        redirect('LoginController/index', 'refresh');
	    }
	}

	public function lista(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$data['contenido'] = "compra/lista";
		$compras = $this->CompraModel->getCompras();
		for($i = 0; $i < count($compras); $i++){
			$compras[$i]->detalle = $this->DetalleCompraModel->getDetallesCompraByCompra($compras[$i]->codigo);
		}
		$data['compras'] = $compras;
		$this->load->view("template/template", $data);
	}

	public function create(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$data['contenido'] = "compra/create";
		$data['proveedores'] = $this->ProveedorModel->getProveedores();
		$data['empleados'] = $this->EmpleadoModel->getEmpleados();
		$data['articulos'] = $this->ArticuloModel->getArticulos();

		$this->load->view("template/template", $data);
	}

	public function update(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$idProveedor = $_GET["idProveedor"];
		$data['contenido'] = "proveedor/update";
		$data['proveedor'] = $this->ProveedorModel->getProveedor($idProveedor);
		$data['provincias'] = $this->ProvinciaModel->getProvincias();
		$data['localidades'] = $this->LocalidadModel->getLocalidades();
		$data['categorias'] = $this->CategoriaModel->getCategorias();
		$data['domicilio'] = $this->DomicilioModel->getDomicilio($data['proveedor'][0]->domicilio);
		$this->load->view("template/template", $data);
	}

	public function eliminarProveedor(){
		$idProveedor = $_GET["idProveedor"];
		$this->ProveedorModel->deleteProveedor($idProveedor);

		redirect(base_url() . 'index.php/ProveedorController/lista');
	}

	
	public function guardarCompra(){
		$idProveedor = $_POST["proveedor"];
		$idEmpleado = $_POST["empleado"];

		$count = 0;
		foreach($_POST["articuloDetalle"] as $articulo){
			$detalles[$count]['articulo'] = $articulo;
			$count++;
		}

		$count = 0;
		foreach($_POST["cantidadDetalle"] as $cantidad){
			$detalles[$count]['cantidad'] = $cantidad;
			$count++;
		}

		$compra_data = array(
				'fecha' => date('Y-m-d H:i:s'),
				'proveedor' => $idProveedor,
				'empleado' => $idEmpleado
			);

		$idCompra = $this->CompraModel->insertCompra($compra_data);

		foreach($detalles as $detalle){
			$detalle_data = array(
					'articulo' => $detalle['articulo'],
					'compra' => $idCompra,
					'cantidad' => $detalle['cantidad']
				);
			$this->DetalleCompraModel->insertDetalleCompra($detalle_data);
		}
		
		redirect(base_url() . 'index.php/CompraController/lista'); 
	}
}