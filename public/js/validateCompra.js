jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
      var element = $(this).data("href");
      if($(element).is(":visible")){
        $(element).hide();
      } else{
        $(element).show();
      }
    });
});

function validate(id) {
  	var campos = "";
  	/*
  	if(!$("#nombre").val())
  		campos = campos + "Nombre \n";
    if(!$("#descripcion").val())
      campos = campos + "Descripcion \n";
    if(!$("#precio").val())
      campos = campos + "Precio \n";
    if(!$("#stock").val())
      campos = campos + "Stock \n";
  	if($("#categoria").val() == 0)
  		campos = campos + "Categoria \n";
  	*/
  	if(campos.length === 0){
  		$('form#guardarCompraForm').submit();
  	}else{
  		swal("Completar los siguientes campos: ", campos);
  	}
}

function confirmDelete(id) {
    swal({
        title: "Eliminar Articulo",
        text: "¿Está seguro que desea eliminar este Articulo?",
        type: "error",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Eliminar!',
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true
      },
      function(isConfirm){
        if (isConfirm) {
              window.location.href = "../ArticuloController/eliminarArticulo?idArticulo=" + id;
        }
    });
}

function agregarDetalle(){
  var num = parseInt($('#tablaDetalle tr:last').attr('id').split('-')[1]); // how many "duplicatable" input fields we currently have
  var newNum = new Number(num + 1); // the numeric ID of the new input field being added

  // create the new element via clone(), and manipulate it's ID using newNum value
  var newElem = $('#detalleRow-0').clone().attr('id', 'detalleRow-' + newNum);
  newElem.show();

  // manipulate the name/id values of the input inside the new element
  newElem.find('#articuloDetalle0').attr('id', 'articuloDetalle' + newNum).prop('disabled', false);
  newElem.find('#cantidadDetalle0').attr('id', 'cantidadDetalle' + newNum).prop('disabled', false);



  // insert the new element after the last "duplicatable" input field
  $('#detalleRow-' + num).after(newElem);

  newElem.find('#articuloDetalle' + newNum).select2();

  // business rule: you can only add 10 names
    if ($('.clonedRow').length == 10)
      $('#btnAgregarDetalle').attr('disabled','disabled');
}

function quitarDetalle(row){
  if($('.clonedRow').length > 1){
    $(row).parent().parent().remove();
    $('#btnAgregarDetalle').attr('disabled',false);
  }else{
    swal("Informacion", "Se debe cargar al menos un detalle de compra");
  }
}