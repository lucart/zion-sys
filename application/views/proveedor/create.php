<div class="box box-warning">
  <div class="box-header with-border">
    <h3 class="box-title">Crear un Nuevo Proveedor</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <form id="guardarProveedorForm" role="form" action="../ProveedorController/guardarProveedor" method="post" >		
              <table class="table table-hover">
                <tr>
                  <td width="25%"><!-- text input -->
					  <div class="form-group">
						<label>Nombre</label>
						<input type="text" id="nombre" name="nombre" class="form-control" placeholder="Ingrese el Nombre del Proveedor">
					  </div>
				  </td>
				  <td width="5%"></td>				  
                  <td width="25%">
					<!-- text input -->
					  <div class="form-group">
						<label>Razon Social</label>
						<input type="text" id="razon_social" name="razon_social" class="form-control" placeholder="Ingrese la razon social del Proveedor">
					  </div>
				  </td>	
				  <td width="45%"></td>	
                </tr>
              </table>
			  <div class="panel-group">
				  <div class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" href="#collapse1">Dirección</a>
					  </h4>
					</div>
					<div id="collapse1" class="panel-collapse collapse">					  
						<table class="table table-hover">
							<tr>                  
							  <td width="25%">
								<!-- select input -->
								  <div class="form-group">
									<label>Provincia</label>
									<select id="provincia" name="provincia" class="form-control" onchange="cargarLocalidades();" >							
										<?php
											for ($i = 0; $i < count($provincias); $i++) {
										?>
										<option  value="<?php echo $provincias[$i]->codigo?>"><?php echo $provincias[$i]->nombre?></option>
										
										<?php } ?>
									</select>
								  </div>
							  </td>
							  <td width="5%"></td>
							  <td width="25%"> 
								  <!-- select input -->
								  <div class="form-group">
									<label>Localidad</label>
									<select id="localidad" name="localidad" class="form-control" ></select>
								  </div>
								</td>
							  <td width="45%"></td>
							</tr>
							<tr>
							  <td width="25%"><!-- text input -->
								  <div class="form-group">
									<label>Calle</label>
									<input type="text" id="calle" name="calle" class="form-control" placeholder="Ingrese la calle">
								  </div>
							  </td>
							  <td width="5%"></td>
							  <td width="25%">
								<!-- text input -->
								  <div class="form-group">
									<label>Número</label>
									<input type="number" id="numero" name="numero" class="form-control" placeholder="Ingrese el Numero">
								  </div>
							  </td>
							  <td width="45%"></td>
							</tr>
							  <td width="25%"> 
								  <!-- select input -->
								  <div class="form-group">
									<label>Código Postal</label>									
									<input type="text" id="codigo_postal" name="codigo_postal" class="form-control" placeholder="Ingrese el Código Postal del Empleado">
								  </div>
								</td>
								<td width="5%"></td>
								<td width="25%"></td>
							  <td width="45%"></td>
							</tr>
						</table>	
					</div>
				  </div>
				</div>	
			  <table class="table table-hover">
                <tr>
				  <td width="25%">
				  	<div class="form-group">
						<label>Categoria</label>
						<select id="categoria" name="categoria" class="form-control">							
							<?php
								for ($i = 0; $i < count($categorias); $i++) {
							?>
							<option  value="<?php echo $categorias[$i]->codigo?>"><?php echo $categorias[$i]->nombre?></option>
							<?php } ?>
						</select>
					  </div>
				  </td>
				  <td width="5%"></td>
				  <td width="25%"></td>
				  <td width="45%"></td> 		
                </tr>
              </table>
      <div class="box-footer">
        <button type="button" onClick="validate();" class="btn btn-primary">Guardar</button>
      </div>

      
    </form>
  </div>
  <!-- /.box-body -->
</div>
<script src="<?php echo base_url('public/js/validateProveedor.js')?>"></script>
<script>
	cargarLocalidades( );
	
	function cargarLocalidades( ){
		var provincia=document.getElementById("provincia").value;
		var localidad=document.getElementById("localidad");
		localidad.options.length =0;
		
		<?php for($i = 0; $i < count($localidades); $i++){?>
			if(<?php echo $localidades[$i]->provincia; ?> == provincia){				
				var option = document.createElement("option");
				option.value = <?php echo $localidades[$i]->codigo; ?>;
				option.text = <?php echo "'".$localidades[$i]->nombre."'"; ?>;
				localidad.add(option);
			}			
		<?php }?>
	}	
</script>