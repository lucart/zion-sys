<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Editar Categoria</h3>
  </div>
  <div class="box-body">
    <form id="guardarCategoriaForm" role="form" action="../CategoriaController/guardarCategoria" method="post" >
      <input type="hidden" name="idCategoria" value="<?php echo $categoria[0]->codigo?>">    
        <table class="table table-hover">
          <h3>Comedor</h3>
          <tr>
            <td width="25%">      
              <div class="form-group">
              <label>Nombre</label>
              <input type="text" id="nombre" name="nombre" value="<?php echo $categoria[0]->nombre?>" class="form-control" placeholder="Ingrese el Nombre de la categoria">
              </div>
            </td>
            <td width="45%"></td>
          </tr>
          <tr>
            <td colspan="3">
              <div class="form-group">
              <label>Descripcion</label>
              <textarea class="form-control" id="descripcion" name="descripcion" rows="5" placeholder="Ingrese la descripcion"><?php echo $categoria[0]->descripcion?></textarea>
              </div>
            </td>
          </tr>
        </table>
        <div class="box-footer">
          <button type="button" onClick="validate();" class="btn btn-primary">Guardar</button>
        </div>
    </form>
  </div>
</div>
<script src="<?php echo base_url('public/js/validateCategoria.js')?>"></script>