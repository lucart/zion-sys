<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Registrar una nueva Compra</h3>
  </div>
  <div class="box-body">
    <form id="guardarCompraForm" role="form" action="../CompraController/guardarCompra" method="post" >		
	  <table class="table table-hover">
		<tr>
		  <td width="25%">
		  	<div class="form-group">
				<label>Proveedor</label>
				<select id="proveedor" name="proveedor" class="form-control">							
					<?php
						for ($i = 0; $i < count($proveedores); $i++) {
					?>
					<option  value="<?php echo $proveedores[$i]->codigo?>"><?php echo $proveedores[$i]->nombre?></option>
					<?php } ?>
				</select>
			  </div>
		  </td>
		  <td width="5%"></td>
		  <td width="25%">
		  	<div class="form-group">
				<label>Empleado</label>
				<select id="empleado" name="empleado" class="form-control">							
					<?php
						for ($i = 0; $i < count($empleados); $i++) {
					?>
					<option  value="<?php echo $empleados[$i]->codigo?>"><?php echo $empleados[$i]->nombre?></option>
					<?php } ?>
				</select>
			  </div>
		  </td>
		  <td width="45%"></td> 		
        </tr>
	  </table>

	<!-- Listado -->
		<div class="row">
		  <div class="col-xs-12">
		    <div class="box">
		      <div class="box-header">
		        <h3 class="box-title">Detalle</h3>
		      </div>
		      
		      <!-- /.box-header -->
		      <div class="box-body table-responsive no-padding">
		        <table id="tablaDetalle" class="table table-hover">
		          <tr>
		            <th>Articulo</th>
		            <th>Cantidad</th>
		          </tr>
		          <tr id="detalleRow-0" class="clonedRow" style="display: none">
		            <td width="40%">
		            	  <div class="form-group" style="margin-bottom: 0px;">
							<select id="articuloDetalle0" name="articuloDetalle[]" class="form-control select2" disabled>							
								<?php
									for ($i = 0; $i < count($articulos); $i++) {
								?>
								<option  value="<?php echo $articulos[$i]->codigo?>"><?php echo $articulos[$i]->nombre?></option>
								<?php } ?>
							</select>
						  </div>
		            </td>
		            <td width="10%">
		              <div class="form-group"  style="margin-bottom: 0px;">
						<input type="text" id="cantidadDetalle0" name="cantidadDetalle[]" class="form-control" disabled>
					  </div>
		            </td>
		            <td>
		            	<button type="button" onclick="quitarDetalle(this);" class="btn btn-danger glyphicon glyphicon-trash btn-xs" style="margin-left: 10px;"></button>
		            </td>
					  <td width="50%"></td> 
		          </tr>
		        </table>
		        <button style="margin-left: 8px;" type="button" id="btnAgregarDetalle" onClick="agregarDetalle();" class="btn btn-success btn-sm">Agregar</button>
		      </div>
		      <!-- /.box-body -->
		    </div>
		    <!-- /.box -->
		  </div>
		</div>
	<!-- End Listado -->

      <div class="box-footer">
        <button type="button" onClick="validate();" class="btn btn-primary">Guardar</button>
      </div>
    </form>
  </div>
</div>
<script src="<?php echo base_url('public/plugins/jQuery/jquery-2.2.3.min.js')?>"></script>
<script src="<?php echo base_url('public/plugins/select2/select2.full.min.js')?>"></script>
<script src="<?php echo base_url('public/js/validateCompra.js')?>"></script>