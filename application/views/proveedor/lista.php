<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Listado De Proveedores</h3>

        <div class="box-tools">
          <div class="input-group input-group-sm" style="width: 150px;">
            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

            <div class="input-group-btn">
              <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            </div>
          </div>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tr>
            <th>Código</th>
            <th>Nombre</th>
            <th>Razón Social</th>
            <th>Provincia</th>
            <th>Localidad</th>
            <th>Categoría</th>
            <th>Acciones</th>
          </tr>
	<?php
		for ($i = 0; $i < count($proveedores); $i++) {
	?>
          <tr>
            <td><?php echo $proveedores[$i]->codigo;  ?></td>
            <td><?php echo $proveedores[$i]->nombre;  ?></td>
            <td><?php echo $proveedores[$i]->razon_social;  ?></td>
            <td><?php echo $proveedores[$i]->provincia;  ?></td>
            <td><?php echo $proveedores[$i]->localidad;  ?></td>
            <td><?php echo $proveedores[$i]->categoria;  ?></td>
            <td><button onclick="window.location.href='../ProveedorController/update?idProveedor=<?php echo $proveedores[$i]->codigo;?>'" class="btn btn-warning glyphicon glyphicon-pencil btn-xs"></button>
                  <button onclick="confirmDelete(<?php echo $proveedores[$i]->codigo;?>);" class="btn btn-danger glyphicon glyphicon-trash btn-xs" style="margin-left: 10px;"></button>
            </td>
          </tr>
	<?php } ?>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>
<script src="<?php echo base_url('public/js/validateProveedor.js')?>"></script>