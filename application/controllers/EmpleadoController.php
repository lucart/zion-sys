<?php

class EmpleadoController extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('EmpleadoModel');
		$this->load->model('PersonaModel');
		$this->load->model('ProvinciaModel');
		$this->load->model('LocalidadModel');
		$this->load->model('DomicilioModel');
		$this->load->model('CargoModel');
		error_reporting(E_ERROR | E_PARSE);
		if(empty($this->session->userdata("logged_in")))
        {
	        redirect('LoginController/index', 'refresh');
	    }
	}

	public function lista(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$data['contenido'] = "empleado/lista";
		$data['empleados'] = $this->EmpleadoModel->getEmpleados();
		$this->load->view("template/template", $data);
	}

	public function create(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$data['contenido'] = "empleado/create";
		$data['provincias'] = $this->ProvinciaModel->getProvincias();
		$data['localidades'] = $this->LocalidadModel->getLocalidades();
		$data['cargos'] = $this->CargoModel->getCargos();
		$this->load->view("template/template", $data);
	}

	public function update(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$idEmpleado = $_GET["idEmpleado"];
		$data['contenido'] = "empleado/update";
		$data['empleado'] = $this->EmpleadoModel->getEmpleado($idEmpleado);
		$data['provincias'] = $this->ProvinciaModel->getProvincias();
		$data['localidades'] = $this->LocalidadModel->getLocalidades();
		$data['cargos'] = $this->CargoModel->getCargos();
		$data['domicilio'] = $this->DomicilioModel->getDomicilio($data['empleado'][0]->domicilio);
		$this->load->view("template/template", $data);
	}

	public function eliminarEmpleado(){
		$idEmpleado = $_GET["idEmpleado"];
		$this->EmpleadoModel->deleteEmpleado($idEmpleado);

		redirect(base_url() . 'index.php/EmpleadoController/lista');
	}
	
	public function guardarEmpleado(){
		$idEmpleado = $_POST["idEmpleado"];
		$idPersona = $_POST["idPersona"];
		$idDomicilio = $_POST["idDomicilio"];
		$cuil = $_POST["cuil"];
		$fechaIngreso = $_POST["fecha_ingreso"];
		$idCargo = $_POST["cargo"];
		
		$calle = $_POST["calle"];
		$numero = $_POST["numero"];
		$codigoPostal = $_POST["codigo_postal"];
		$localidad = $_POST["localidad"];

		if($idDomicilio != null){
			$this->DomicilioModel->updateDomicilio($calle, $numero, $codigoPostal, $localidad, $idDomicilio[0]->idDomicilio);
		}else{			
			$idDomicilio = $this->DomicilioModel->insertDomicilio($calle, $numero, $codigoPostal, $localidad);
		}
		
		$nombre = $_POST["nombre"];
		$apellido = $_POST["apellido"];
		$dni = $_POST["dni"];
		$fechaNacimiento = $_POST["fecha_nacimiento"];
		
		if($idPersona != null){
			$this->PersonaModel->updatePersona($nombre, $apellido, $dni, $fechaNacimiento, $idDomicilio[0]->idDomicilio, $idPersona[0]->idPersona);
		}else{		
			$idPersona = $this->PersonaModel->insertPersona($nombre, $apellido, $dni, $fechaNacimiento, $idDomicilio[0]->idDomicilio);
		}
		
		if($idEmpleado == null){
			$this->EmpleadoModel->insertEmpleado($cuil, $fechaIngreso, $idCargo, $idPersona[0]->idPersona);
		}else{
			$this->EmpleadoModel->updateEmpleado($idEmpleado, $cuil, $fechaIngreso, $idCargo, $idPersona[0]->idPersona);
		}

		redirect(base_url() . 'index.php/EmpleadoController/lista');
	}
}