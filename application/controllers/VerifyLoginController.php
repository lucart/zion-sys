<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class VerifyLoginController extends CI_Controller {

    function __construct()
    {
     parent::__construct();
     $this->load->model('UsuariosModel','',TRUE);
    }

    function index()
    {
     //This method will have the credentials validation
     $this->load->library('form_validation');

     $this->form_validation->set_rules('username', 'Username', 'trim|required');
     $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');

     if($this->form_validation->run() == FALSE)
     {
       //Field validation failed.  User redirected to login page
       $this->load->view('login/login');
     }
     else
     {
       //Go to private area
       redirect('HomeController/index', 'refresh');
     }

    }

    function check_database($password)
    {
     //Field validation succeeded.  Validate against database
     $username = $this->input->post('username');   
     //query the database
     $result = $this->UsuariosModel->login($username, $password);
     if($result)
     {
       $sess_array = array();
       foreach($result as $row)
       {
         $sess_array = array(
           'id' => $row->codigo,
           'username' => $row->username,
           'nombre' => $row->nombre,
           'apellido' => $row->apellido,
           'email' => $row->email,
           'permiso' => $row->codigo_permiso
         );
         $this->session->set_userdata('logged_in', $sess_array);         
       }
       return TRUE;
     }
     else
     {
       $this->form_validation->set_message('check_database', 'Nombre de usuario o contraseña inválidos');
       return false;
     }
    }
}
?>