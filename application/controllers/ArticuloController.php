<?php

class ArticuloController extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('ArticuloModel');
		$this->load->model('CategoriaModel');
		error_reporting(E_ERROR | E_PARSE);
		if(empty($this->session->userdata("logged_in")))
	    {
	      redirect('LoginController/index', 'refresh');
	    }
	}

	public function lista(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$data['contenido'] = "articulo/lista";
		$data['articulos'] = $this->ArticuloModel->getArticulos();
		$this->load->view("template/template", $data);
	}

	public function create(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$data['contenido'] = "articulo/create";
		$data['categorias'] = $this->CategoriaModel->getCategorias();
		$this->load->view("template/template", $data);
	}
	
	public function guardarArticulo(){
		$nombre = $_POST["nombre"];
		$categoria = $_POST["categoria"];
		$descripcion = $_POST["descripcion"];
		$precio = $_POST["precio"];
		$stock = $_POST["stock"];
		$idArticulo = $_POST["idArticulo"];

		if($idArticulo != null){
			$this->ArticuloModel->updateArticulo($nombre, $descripcion, $precio, $stock, $categoria, $idArticulo);
		}else{
			$this->ArticuloModel->insertArticulo($nombre, $descripcion, $precio, $stock, $categoria);
		}
				
		redirect(base_url() . 'index.php/ArticuloController/lista');
	}

	public function eliminarArticulo(){
		$idArticulo = $_GET["idArticulo"];
		$this->ArticuloModel->deleteArticulo($idArticulo);

		redirect(base_url() . 'index.php/ArticuloController/lista');
	}

	public function update(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$idArticulo = $_GET["idArticulo"];
		$data['contenido'] = "articulo/update";
		$data['articulo'] = $this->ArticuloModel->getArticulo($idArticulo);
		$data['categorias'] = $this->CategoriaModel->getCategorias();
		$this->load->view("template/template", $data);
	}

}