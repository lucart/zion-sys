<?php

class PersonaModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getPersnas() {
		$query = $this->db->query("select * from PERSONA");
		return $query->result();
	}

	public function getPersona($id) {
		$query = $this->db->query("select * from PERSONA where codigo = $id");
		return $query->result();
	}
	
	public function insertPersona($nombre, $apellido, $dni, $fechaNacimiento, $idDomicilio) {
		$queryIns = $this->db->query("INSERT INTO `PERSONA` ( `nombre`, `apellido`,`dni`,`fecha_nacimiento`,`domicilio`) 
									VALUES ('$nombre','$apellido',$dni, '$fechaNacimiento', $idDomicilio);");								
		$query = $this->db->query("SELECT MAX(codigo) as idPersona from PERSONA;");
		
		return $query->result();
	}

	public function updatePersona($nombre, $apellido, $dni, $fechaNacimiento, $idDomicilio, $idPersona) {
      $queryUpdate = $this->db->query("update personas
                                  set nombre = '$nombre',
                                      apellido = '$apellido',
                                      dni = $dni,
                                      fecha_nacimiento = '$fechaNacimiento',
                                      domicilio = $idDomicilio
                                      where codigo = $idPersona;");

      $query = $this->db->query("SELECT MAX(codigo) as idPersona from PERSONA;");
		
	  return $query->result();
	}
}