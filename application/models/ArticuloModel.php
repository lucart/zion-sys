<?php

class ArticuloModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getArticulos() {
		$query = $this->db->query("select ar.codigo, ar.nombre, ar.descripcion, ar.precio, ar.stock, ca.nombre categoria 
									from `ARTICULO` ar, `CATEGORIA` ca 
									where ar.categoria = ca.codigo");
		return $query->result();
	}

	public function getArticulo($id) {
		$query = $this->db->query("select codigo, nombre, descripcion, precio, stock, categoria from `ARTICULO` where codigo = $id");
		return $query->result();
	}
	
	public function insertArticulo($nombre, $descripcion, $precio, $stock, $categoria) {
		$query = $this->db->query("INSERT INTO `ARTICULO` ( `nombre`, `descripcion`, `precio`, `stock`, `categoria`) 
									VALUES ('$nombre','$descripcion','$precio','$stock','$categoria');");
		return true;
	}

	public function deleteArticulo($id) {
      $query = $this->db->query("delete from ARTICULO where codigo = $id");
      
      if($query){
        return true;
      }
      return false;
    }

    public function updateArticulo($nombre, $descripcion, $precio, $stock, $categoria, $idArticulo) {
      $query = $this->db->query("update ARTICULO
                                  set nombre = '$nombre',
                                      descripcion = '$descripcion',
                                      precio = $precio,
                                      stock = $stock,
                                      categoria = $categoria
                                      where codigo = $idArticulo;");
      return true;
    }
}