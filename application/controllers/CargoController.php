<?php

class CargoController extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('CargoModel');
		error_reporting(E_ERROR | E_PARSE);
		if(empty($this->session->userdata("logged_in")))
	    {
	      redirect('LoginController/index', 'refresh');
	    }
	}

	public function lista(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$data['contenido'] = "cargo/lista";
		$data['cargos'] = $this->CargoModel->getCargos();
		$this->load->view("template/template", $data);
	}

	public function create(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$data['contenido'] = "cargo/create";
		$this->load->view("template/template", $data);
	}
	
	public function guardarCargo(){
		$nombre = $_POST["nombre"];
		$descripcion = $_POST["descripcion"];
		$idCargo = $_POST["idCargo"];

		if($idCargo != null){
			$this->CargoModel->updateCargo($nombre, $descripcion, $idCargo);
		}else{
			$this->CargoModel->insertCargo($nombre, $descripcion);
		}
				
		redirect(base_url() . 'index.php/CargoController/lista');
	}

	public function eliminarCargo(){
		$idCargo = $_GET["idCargo"];
		$this->CargoModel->deleteCargo($idCargo);

		redirect(base_url() . 'index.php/CargoController/lista');
	}

	public function update(){
		$data = validateSessionData($this->session->userdata('logged_in'));
		$idCargo = $_GET["idCargo"];
		$data['contenido'] = "cargo/update";
		$data['cargo'] = $this->CargoModel->getCargo($idCargo);
		$this->load->view("template/template", $data);
	}

}