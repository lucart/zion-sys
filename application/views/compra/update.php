<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Editar Articulo</h3>
  </div>
  <div class="box-body">
    <form id="guardarArticuloForm" role="form" action="../ArticuloController/guardarArticulo" method="post" >
      <input type="hidden" name="idArticulo" value="<?php echo $articulo[0]->codigo?>">    
        <table class="table table-hover">
          <h3>Articulo</h3>
          <tr>
            <td colspan="2" width="20%">      
              <div class="form-group">
              <label>Nombre</label>
              <input type="text" id="nombre" name="nombre" value="<?php echo $articulo[0]->nombre?>" class="form-control" placeholder="Ingrese el Nombre del Articulo">
              </div>
            </td>
            <td width="20%"></td>
            <td width="60%"></td>
          </tr>
           <tr>
            <td colspan="2" width="20%">
              <div class="form-group">
              <label>Categoría</label>
              <select id="categoria" name="categoria" class="form-control" >  
                <option  value="0">Seleccione...</option>
                <?php
                for ($i = 0; $i < count($categorias); $i++) {
                  if($categorias[$i]->codigo == $articulo[0]->categoria) {
                ?>
                  <option selected value="<?php echo $categorias[$i]->codigo?>"><?php echo $categorias[$i]->nombre?></option>
                <?php } else {?>
                  <option value="<?php echo $categorias[$i]->codigo?>"><?php echo $categorias[$i]->nombre?></option>
                 <?php } } ?>
                </select>
              </div>
            </td>
            <td width="20%"></td>
            <td width="60%"></td>
          </tr>
          <tr>
            <td colspan="2">
              <div class="form-group">
              <label>Descripcion</label>
              <textarea class="form-control" id="descripcion" name="descripcion" rows="5" placeholder="Ingrese la descripcion"><?php echo $articulo[0]->descripcion?></textarea>
              </div>
            </td>
          </tr>
          <tr>
            <td width="20%">      
              <div class="form-group">
              <label>Precio</label>
              <input type="text" id="precio" name="precio" class="form-control" value="<?php echo $articulo[0]->precio?>" placeholder="Ingrese el Precio del Articulo">
              </div>
            </td>
            <td width="20%">      
              <div class="form-group">
              <label>Stock</label>
              <input type="text" id="stock" name="stock" class="form-control" value="<?php echo $articulo[0]->stock?>" placeholder="Ingrese el Stock del Articulo">
              </div>
            </td>
            <td width="60%"></td>
          </tr>
        </table>
        <div class="box-footer">
          <button type="button" onClick="validate();" class="btn btn-primary">Guardar</button>
        </div>
    </form>
  </div>
</div>
<script src="<?php echo base_url('public/js/validateArticulo.js')?>"></script>