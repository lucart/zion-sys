<?php

class EmpleadoModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getEmpleados() {
		$query = $this->db->query("select emp.codigo, emp.cuil, emp.fecha_ingreso, ca.nombre cargo, 
									pe.nombre, pe.apellido, pe.dni, pe.fecha_nacimiento 
									from EMPLEADO emp, PERSONA pe, CARGO ca 
									where emp.cargo = ca.codigo 
									and emp.persona = pe.codigo");
		return $query->result();
	}

	public function getEmpleado($id) {
		$query = $this->db->query("select co.codigo, co.cuil, co.fecha_ingreso, co.cargo, 
									pe.nombre, pe.apellido, pe.dni, pe.fecha_nacimiento,  
									pe.domicilio, do.localidad, lo.provincia 
									from EMPLEADO co, PERSONA pe, DOMICILIO do, LOCALIDAD lo 
									where co.codigo = $id 
									and co.persona = pe.codigo 
									and pe.domicilio = do.codigo 
									and do.localidad = lo.codigo");
		return $query->result();
	}
	
	public function insertEmpleado($cuil, $fechaIngreso, $idCargo, $idPersona) {
		$query = $this->db->query("INSERT INTO `EMPLEADO` ( `cuil`, `fecha_ingreso`, `cargo`, `persona`) 
									VALUES ('$cuil','$fechaIngreso','$idCargo','$idPersona');");
		return true;
	}

	public function updateEmpleado($idEmpleado, $cuil, $fechaIngreso, $idCargo, $idPersona) {
      $query = $this->db->query("update empleado
                                  set cuil = '$cuil',
                                      fecha_ingreso = '$fechaIngreso',
                                      cargo = $idCargo, 
                                      persona = $idPersona
                                      where codigo = $idEmpleado;");
      return true;
    }
	
	public function deleteEmpleado($id) {
      $query = $this->db->query("delete from EMPLEADO where codigo = $id");
      
      if($query){
        return true;
      }
      return false;
    }
}